/*
* Copyright (c) 2014 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import Foundation
import UIKit
import CloudKit

protocol NearbyLocationsResultsControllerDelegate {
    func willBeginUpdating()
    func establishmentAdded(establishment: Establishment, index: Int)
    func establishmentUpdated(establishment: Establishment, index: Int)
    func didEndUpdating(error: NSError!)
}

class NearbyLocationsResultsController {
    
    let db: CKDatabase //1
    var predicate = NSPredicate()
    let delegate: NearbyLocationsResultsControllerDelegate //3
    var results = [Establishment]() //4
    
    var resultLimit = 30
    var cursor: CKQueryCursor!
    var startedGettingResults = false
    let RecordType = "Establishmet"
    var inProgress = false
    
    var subscriptionID = "subscription_id"
    var subscribed = false
    
    init(delegate: NearbyLocationsResultsControllerDelegate) {
        self.delegate = delegate
        db = CKContainer.defaultContainer().publicCloudDatabase
    }
    
    func start() {
        if inProgress {
            return
        }
        
        inProgress = true
        let predicate = NSPredicate(value: true) //TODO: Let's just fetch them all for now
        let query = CKQuery(recordType: RecordType, predicate: predicate)
        let queryOp = CKQueryOperation(query: query)
        
        sendOperation(queryOp)
        
        subscribe()
    }
    
    func recordFetched(record: CKRecord!) {
        if !startedGettingResults {
            startedGettingResults = true
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.delegate.willBeginUpdating()
            })
        }
        
        var index = NSNotFound
        var e: Establishment!
        var newItem = true
        
        for (idx, value) in results.enumerate() {
            if value.record.recordID == record.recordID {
                index = idx
                e = value
                e.record = record
                newItem = false
                break
            }
        }
        
        if index == NSNotFound {
            e = Establishment(record: record, database: db)
            results.append(e)
            index = results.count - 1
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            if newItem {
                self.delegate.establishmentAdded(e, index: index)
            } else {
                self.delegate.establishmentUpdated(e, index: index)
            }
        }
    }
    
    func queryCompleted(cursor: CKQueryCursor!, error: NSError!) {
        startedGettingResults = false
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.delegate.didEndUpdating(error)
        }
    }
    
    func fetchNextResults(cursor: CKQueryCursor) {
        let queryOp = CKQueryOperation(cursor: cursor)
        sendOperation(queryOp)
    }
    
    func sendOperation(queryOp: CKQueryOperation) {
        queryOp.queryCompletionBlock = { cursor, error in
            self.queryCompleted(cursor, error: error)
            if cursor != nil {
                self.fetchNextResults(cursor!)
            } else {
                self.inProgress = false
            }
        }
        
        queryOp.recordFetchedBlock = { record in
            self.recordFetched(record)
        }
        
        queryOp.resultsLimit = resultLimit
        startedGettingResults = false
        db.addOperation(queryOp)
        
    }
    
    func subscribe() {
        if subscribed {
            return
        }
        
        let options = CKSubscriptionOptions.FiresOnRecordCreation.rawValue | CKSubscriptionOptions.FiresOnRecordDeletion.rawValue | CKSubscriptionOptions.FiresOnRecordUpdate.rawValue
        
        let predicate = NSPredicate(value: true) //TODO: Let's just fetch them all for now
        let subscription = CKSubscription(recordType: RecordType, predicate: predicate, subscriptionID: subscriptionID, options: CKSubscriptionOptions(rawValue: options))
        subscription.notificationInfo = CKNotificationInfo()
        subscription.notificationInfo?.alertBody = ""
        
        db.saveSubscription(subscription) { (subscription, error) -> Void in
            guard error == nil else {
                print("error subscribing: \(error!)")
                return
            }
            
            self.subscribed = true
            print("subscribed")
        }
    }
}
